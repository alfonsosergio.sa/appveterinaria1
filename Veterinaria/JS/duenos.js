
const listaVeteri = document.getElementById("lista-veterinarios");
const pais = document.getElementById("pais");
const nombre = document.getElementById("nombre");
const identificacion = document.getElementById("identificacion");
const apellido = document.getElementById("apellido");
const indice = document.getElementById("indice");
const form = document.getElementById("form");
const btnGuardar = document.getElementById("btn-guardar");
//const form = document.getElementsByClassName("form");
//const btnGuardar = document.getElementsById("btn-guardar");

let veterinarios = [
    {
        nombre: "Sergio",
        identificacion: "123456",
        apellido: "Alfonso",
        pais: "Argentina",
    },
    {
        nombre: "Sergio",
        identificacion: "123456",
        apellido: "Alfonso",
        pais: "Argentina",
    },
    {
        nombre: "Sergio",
        identificacion: "123456",
        apellido: "Alfonso",
        pais: "Argentina",
    },
    {
        nombre: "Sergio",
        identificacion: "123456",
        apellido: "Alfonso",
        pais: "Argentina",
    },
];

function listarVeteri(){
    
    let htmlVeterinarios = veterinarios.map((veterinario, index)=> `<tr>
    <th scope="row">${index}</th>
    <td>${veterinario.nombre}</td>
    <td>${veterinario.identificacion}</td>
    <td>${veterinario.apellido}</td>
    <td>${veterinario.pais}</td>
    <td>
        <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button" class="btn btn btn-info editar" data-toggle="modal" data-target="#exampleModal"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
              </svg></button>
            <button type="button" class="btn btn-danger eliminar"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
              </svg></button>
          </div>
    </td>
  </tr>`).join("");
  listaVeteri.innerHTML = htmlVeterinarios;
  Array.from(document.getElementsByClassName("editar")).forEach((botonEditar, index) => botonEditar.onclick = editar(index))
  Array.from(document.getElementsByClassName("eliminar")).forEach((botonEliminar, index) => botonEliminar.onclick = eliminar(index)
  )
};



listarVeteri();



function enviarDatos (evento) {
    
    const datos = {
        nombre: nombre.value,
        identificacion: identificacion.value,
        apellido: apellido.value,
        pais: pais.value,
    }

    const accion = btnGuardar.innerHTML;
    switch (accion) {
        case "Editar":
            veterinarios[indice.value] = datos;
            break;
    
        default:
           veterinarios.push(datos);
            break;
    }

   
    
    
    listarVeteri();
    resetModal();
}

function editar(index){
    return function cuandoCliqueo () {
        btnGuardar.innerHTML = "Editar";
        $("#exampleModalCenter").modal("toggle");
        const veterinario = veterinarios[index];
        nombre.value = veterinario.nombre;
        identificacion.value = veterinario.identificacion;
        apellido.value = veterinario.apellido;
        pais.value = veterinario.pais;
        indice.value = index;
        
    };
};

function resetModal() {
        nombre.value = "";
        identificacion.value = "";
        apellido.value = "";
        pais.value = "";
        indice.value = "";
        btnGuardar.innerHTML = "Crear";
}

function eliminar(index) {
    return function clickEnEliminar(){
        veterinarios = veterinarios.filter((veterinario, indiceVeterinario) => indiceVeterinario !==  index);
        listarVeteri();
    }
  
};

form.onsubmit = enviarDatos;
btnGuardar.onclick = enviarDatos;

