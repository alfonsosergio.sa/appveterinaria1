module.exports = global.http.createServer((req, res) => {
    //1. obtener url desde el objeto request
      const urlActual = global.req.url;
      const urlParseada = global.url.parse(urlActual, true);


    //2. obtener la ruta
      const ruta = urlParseada.pathname;


    //3. Quitar Slash
      const rutaLimpia = ruta. replace(/^\/+|\/+$/g, "");


    //3.1 Obtener el metodo
      const metodo =  req.method.toLowerCase();


    //3.2 Obtener variables del query url
    const { query = {} } = urlParseada;


    //3.3 Obtener los headers
    const { headers = {} } = req;


    //3.4 Obtener payload, 
      const decoder = new StringDecoder("utf-8");
      let buffer = "";
   

    //3.4.1 Acumular data cuando request reciba un payload
      req.on("data", (data) => {
        buffer+= decoder.write(data);
      });

      
    //3.4.2 Terminar acumulador de datos
      req.on("end", () => {
        buffer += decoder.end();

        if (headers["content-type"] === "application/json") {
          buffer = JSON.parse(buffer);
        }
      //3.4.3 Revisar si tiene subrutas en este caso es el indice de la página
        if (rutaLimpia.indexOf("/") > -1) {
          var [rutaPrincipal, indice]  = rutaLimpia.split("/");
        }
      //3.5 Ordenar la data del request
        const data = {
          indice,
          ruta: rutaPrincipal || rutaLimpia,
          query,
          metodo,
          headers,
          payload: buffer
        };
        

        console.log({data});

      //3.6 Elegir el manejador dependiendo de la ruta y asignarle el manejador que tiene
      let handler;
      if (data.ruta && enrutador[data.ruta] && enrutador[data.ruta][metodo]) {
        handler = enrutador[data.ruta][metodo];
      } else {
        handler = enrutador.noEncontrado;
      }


      //4. eejecutar handler (manejador) para enviar la respuesta.
      if (typeof handler === "function") {
        handler(data, (statusCode = 200, mensaje)=>{
          const respuesta = JSON. stringify(mensaje);
          res.setHeader("Content-Type", "aplication/json");
          res.writeHead(statusCode);
          
          //Line donde realmente ya estamos respondiendo a la aplicación cliente
          res.end(respuesta);
        })
      } 
    }); 
});

