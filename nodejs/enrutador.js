const mascotas = require("./rutas/mascotas");
const veterinarias = require("./rutas/veterinarias");
const duenos = require("./rutas/duenos");
const consultas = require("./rutas/consultas");

module.exports =  {
    ruta: (data, callback) => {
      callback(200, {mensaje: "esta es /ruta"})
    },
    mascotas,
    noEncontrdo: (data, callback) => {
      callback(404, {mensaje: "no encontrado"});
    },
    veterinarias,
    noEncontrdo: (data, callback) => {
      callback(404, {mensaje: "no encontrado"});
    },
    duenos,
    noEncontrdo: (data, callback) => {
      callback(404, {mensaje: "no encontrado"});
    },
    consultas,
    noEncontrdo: (data, callback) => {
      callback(404, {mensaje: "no encontrado"});
    },
  }