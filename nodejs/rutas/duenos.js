module.exports = {
    get: (data, callback) => {
      if (typeof data.indice !== "undefined") {
        if (global.recursos.duenos[data.indice]) {
          return callback(200, global.recursos.duenos[data.indice]);
        } 
        return callback(404, {mensaje: "duenos con indice ${data.indice} no encontrada"} );
      }
      callback(200, global.recursos.duenos);
    },
    post: (data, callback) => {
      global.recursos.duenos.push(data.payload);
      callback(201, data.payload);
    },
    put: (data, callback) => {
      if (typeof data.indice !== "undefined") {
        if (global.recursos.duenos[data.indice]) {
          global.recursos.duenos[data.indice] = data.payload;
          return callback(200, global.recursos.duenos[data.indice]);
        } 
        return callback(404, {mensaje: "duenos con indice ${data.indice} no encontrada"} );
      }
      callback(400, {mensaje: "indice no enviado"});
    },
    delete: (data, callback) => {
      if (typeof data.indice !== "undefined") {
        if (global.recursos.duenos[data.indice]) {
          global.recursos.duenos = global.recursos.duenos.filter((_duenos, indice) => indice != data.indice
          );
          return callback(204, {mensaje: "elemento con indice ${data.indice} eliminado"});
        } 
        return callback(404, {mensaje: "duenos con indice ${data.indice} no encontrada"} );
      }
      callback(400, {mensaje: "indice no enviado"});
    },
  }; 