module.exports = {
    get: (data, callback) => {
      if (typeof data.indice !== "undefined") {
        if (global.recursos.veterinarias[data.indice]) {
          return callback(200, global.recursos.veterinarias[data.indice]);
        } 
        return callback(404, {mensaje: "veterinarias con indice ${data.indice} no encontrada"} );
      }
      callback(200, global.recursos.veterinarias);
    },
    post: (data, callback) => {
      global.recursos.veterinarias.push(data.payload);
      callback(201, data.payload);
    },
    put: (data, callback) => {
      if (typeof data.indice !== "undefined") {
        if (global.recursos.veterinarias[data.indice]) {
          global.recursos.veterinarias[data.indice] = data.payload;
          return callback(200, global.recursos.veterinarias[data.indice]);
        } 
        return callback(404, {mensaje: "veterinarias con indice ${data.indice} no encontrada"} );
      }
      callback(400, {mensaje: "indice no enviado"});
    },
    delete: (data, callback) => {
      if (typeof data.indice !== "undefined") {
        if (global.recursos.veterinarias[data.indice]) {
          global.recursos.veterinarias = global.recursos.veterinarias.filter((_veterinarias, indice) => indice != data.indice
          );
          return callback(204, {mensaje: "elemento con indice ${data.indice} eliminado"});
        } 
        return callback(404, {mensaje: "veterinarias con indice ${data.indice} no encontrada"} );
      }
      callback(400, {mensaje: "indice no enviado"});
    },
  }; 