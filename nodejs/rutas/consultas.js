module.exports = {
    get: (data, callback) => {
      if (typeof data.indice !== "undefined") {
        if (global.recursos.consultas[data.indice]) {
          return callback(200, global.recursos.consultas[data.indice]);
        } 
        return callback(404, {mensaje: "consultas con indice ${data.indice} no encontrada"} );
      }
      callback(200, global.recursos.consultas);
    },
    post: (data, callback) => {
      global.recursos.consultas.push(data.payload);
      callback(201, data.payload);
    },
    put: (data, callback) => {
      if (typeof data.indice !== "undefined") {
        if (global.recursos.consultas[data.indice]) {
          global.recursos.consultas[data.indice] = data.payload;
          return callback(200, global.recursos.consultas[data.indice]);
        } 
        return callback(404, {mensaje: "consultas con indice ${data.indice} no encontrada"} );
      }
      callback(400, {mensaje: "indice no enviado"});
    },
    delete: (data, callback) => {
      if (typeof data.indice !== "undefined") {
        if (global.recursos.consultas[data.indice]) {
          global.recursos.consultas = global.recursos.consultas.filter((_consultas, indice) => indice != data.indice
          );
          return callback(204, {mensaje: "elemento con indice ${data.indice} eliminado"});
        } 
        return callback(404, {mensaje: "consultas con indice ${data.indice} no encontrada"} );
      }
      callback(400, {mensaje: "indice no enviado"});
    },
  }; 