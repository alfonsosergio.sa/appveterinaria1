const duenos = require("./rutas/duenos");

module.exports = {
    mascotas: [
      
      {tipo: "Gato", nombre: "Ruma0", dueno: "Camilo"},
      {tipo: "Gato", nombre: "Ruma1", dueno: "Camilo"},
      {tipo: "Gato", nombre: "Ruma2", dueno: "Camilo"},
      {tipo: "Gato", nombre: "Ruma3", dueno: "Camilo"},
      {tipo: "Gato", nombre: "Ruma4", dueno: "Camilo"},
      {tipo: "Gato", nombre: "Ruma5", dueno: "Camilo"},
    ],
    veterinarias: [
        {nombre: "Alexandr", apellido: "Perez", documento: "1234567890"},
        {nombre: "Alejandro", apellido: "Mallon", documento: "1234567891"},
        {nombre: "Lucas", apellido: "Calvo", documento: "1234567892"},
    ],
    duenos: [
        {nombre: "Alexandr1", identificacion: "1234560", apellido: "Perez", pais: "Argentina"},
        {nombre: "Alexandr2", identificacion: "1234561", apellido: "Perez", pais: "Argentina"},
        {nombre: "Alexandr3", identificacion: "1234562", apellido: "Perez", pais: "Argentina"},
        {nombre: "Alexandr4", identificacion: "1234563", apellido: "Perez", pais: "Argentina"},
    ],
    consultas: [
      {mascota: 0 , veterinaria: 0 , encabezado: "", historia: "" , diagnostico: ""},
    ],
  }; 